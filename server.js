var express     = require( 'express' ),
	path        = require( 'path' ),
	http        = require( 'http' ),
	fs          = require( 'fs' ),
	socketIO    = require( 'socket.io'),
	osc 		= require( 'node-osc');

var app         = express(),
	server      = http.createServer( app ),
	io          = socketIO.listen( server );

var server_port = 4999;

server.listen(9090);

var users = [];
var max_users = 2;

app.get('/', function ( req, res ) {
	res.sendfile(__dirname + '/public/index.html');
});

app.use( express.static( path.join( __dirname, 'public' ) ) );

io.sockets.on('connection', function ( socket ) {
	var value;






	socket.on( 'imaready', function( data ){
		var slot = -1
		for( var i = 0; i < max_users; i++ ){
			if( !users[ i ] ){
				slot = i;
				break;
			}
		}
		socket.set( 'slot', slot, function(){
			socket.emit('ready');
			users[ slot ] = {
				socket: socket
			};

		});
	});

	socket.on('ping', function( value ) {
		socket.emit('pong', value + 1);
	});

	socket.on('acc', function( data ){
		socket.get( 'slot', function( err, slot ){
			socket.broadcast.emit( 'cntrl', data );

		});
	});

	socket.on('trigger', function( data ){
		socket.get( 'slot', function( err, slot ){
			//socket.broadcast.emit( 'cntrl', data );
			socket.broadcast.emit( 'trigger', data );

		});


	});

	socket.on('filterChannel', function( data ){
		socket.get( 'slot', function( err, slot ){
			//socket.broadcast.emit( 'cntrl', data );
			socket.broadcast.emit( 'filterChannel', data );

		});


	});

	socket.on('trigger_end', function( data ){
		socket.get( 'slot', function( err, slot ){
			socket.broadcast.emit( 'trigger_end', data );

		});
	});

	socket.on('disconnect', function(){
		console.log( 'client disconnected' );

		for( var i = 0; i < max_users; i++ ){
			if( users[ i ] && users[ i ].socket == this ){
				users[ i ] = null;
				console.log( 'client nulled' );
				break;
			}
		}
	});
});



// udp
var client = new osc.Client('localhost', server_port);

function sendOSC( path, value ){
	client.send( path, value );
}