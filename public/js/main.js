$(function() {
  var buttons = [];
  
  Klang.init("http://klangfiles.s3.amazonaws.com/uploads/projects/NVlE4/config.json", function() {
    setUpButtons();
    Klang.triggerEvent("start_track");
  })
  
  function setUpButtons() {
    var a = new Interface.Panel({ 
      container:$(".buttonPanel") 
    });
    a.background = 'black';   
    var labels = ["drums", "bass", "chords", "vocals"];
    for (var i= 0; i<labels.length; i++) {
      var b = new Interface.Button({ 
        bounds:[i*1/labels.length,.01,1/labels.length-0.001,0.9],  
        label:labels[i],
        onvaluechange: function () {
            Klang.triggerEvent(this.label, this.value);
        }
      });
      a.add(b);
      buttons[labels[i]] = b;
    }
  }

});